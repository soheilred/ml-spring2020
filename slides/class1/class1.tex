\documentclass{beamer}

\let\val\undefined
\usepackage{pgf}
\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{booktabs}
\usepackage{natbib}
\usepackage{algorithm2e}
\usepackage{siunitx}
\usepackage{framed}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{bigdelim,multirow}
\usepackage{grffile}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{libertine}

\usetikzlibrary{arrows,automata,backgrounds,positioning,decorations,intersections,matrix}

% *** Styles ***
%\setbeamertemplate{navigation symbols}{}
\usecolortheme{dolphin}
%\usecolortheme{rose}
\setbeamercovered{transparent}
\usefonttheme{professionalfonts}
%\usefonttheme[onlymath]{serif}

% *** Colors ***
\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}

\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;} 

\newcommand{\Ex}{\mathbb{E}}
%\newcommand{\Pr}{\mathbb{P}}
\DeclareMathOperator{\Var}{Var}

\definecolor{varcolor}{RGB}{132,23,49}
\newcommand{\varname}[1]{\textcolor{varcolor}{\mathsf{#1}}}

\title{Machine Learning}
\subtitle{Introduction to Machine Learning}
\author{Marek Petrik}
\date{January 22, 2020}

\begin{document}
	\begin{frame}
		\maketitle
		\tiny{Some of the figures in these slides are taken from "An Introduction to Statistical Learning, with applications in R"  (Springer, 2013) with permission from the authors: G. James, D. Witten,  T. Hastie and R. Tibshirani }
	
	\end{frame}
	
	\begin{frame} \frametitle{What is machine learning?}		
		Arthur Samuel (1959, IBM, computer checkers): 
		\begin{quote} 
			Field of study that gives computers the ability to learn without being explicitly programmed
		\end{quote}		
		
		\begin{center}
			\includegraphics[width=0.6\linewidth]{../figs/class1/checkers.jpg}\\
			{\tiny Creative commons license, Source: \url{http://flickr.com}}
		\end{center}
	\end{frame}

	\begin{frame} \frametitle{IBM Watson: Computers Beat Humans in Jeopardy}
		\centering
		\includegraphics[width=0.8\linewidth]{../figs/class1/Watson_Jeopardy.jpg} \\
		\tiny{Fair use, https://en.wikipedia.org/w/index.php?curid=31142331}
	\end{frame}

	\begin{frame} \frametitle{AlphaGo: Computers Beat Humans in Go}
		\centering
		\includegraphics[width=0.8\linewidth]{../figs/class1/go_image.jpg} \\
		\tiny{Photograph by Saran Poroong — Getty Images/iStockphoto }
	\end{frame}

	\begin{frame} \frametitle{Personalized Product Recommendations}
		\centering
		Online retailers mine purchase history to make recommendations\\ [3mm]
		\includegraphics[width=0.75\linewidth]{../figs/class1/recommendation.png}
	\end{frame}
	
		
	\begin{frame} \frametitle{Predicting Strength of Hurricanes}
		\centering
		\emph{NOAA Models}: \texttt{SHIFOR,SHIPS,DSHIPS,LGEM} \\ [3mm]
		\includegraphics[width=0.8\linewidth]{../figs/class1/hurricane_image.jpg} \\
		\tiny{Hurricane Isabel, 2003, Source: NOAA.gov }
	\end{frame}
	
	\begin{frame} \frametitle{Other Applications}
		\begin{enumerate}
			\item Health-care: Identify risks of getting a disease
			\item Detect spam in emails
			\item Recognize hand-written text
			\item Create a fake video (\url{https://www.youtube.com/watch?v=cQ54GDm1eL0})
		\end{enumerate}
	
		\alert{Any other applications?}	
	\end{frame}	
	
	\begin{frame}{This Course: User Guide to Machine Learning}
		\begin{itemize}
			\item \textbf{Practical tools}: When and how to use each method
			\vfill
			\item \textbf{How (not) to fail}: How things can go wrong
			\vfill
			\item \textbf{Focus on}: What and Why of ML
			\vfill
			\item \textbf{New this year}: More focus and optional assignments on more rigorous analysis
		\end{itemize}	
	\end{frame}

	\begin{frame}{This course is not for you if you ...}
	
		\begin{enumerate}
			\item Want to (only) develop new ML methods: CS 757/857 is better
			\vfill
			\item Care about vision: CS 780/880 (Vision) is better
			\vfill			
			\item Care about robotics: CS 733/833 is better
			\vfill			
			\item Care about text: CS 753/835 is better
		\end{enumerate}

	\end{frame}

	\begin{frame}\frametitle{Logistics}
		\begin{itemize}
			\item \textbf{Website}:  (get there through mycourses) \\ {\small\url{https://gitlab.com/marekpetrik/ml-spring2020}}
			\item \textbf{Grading}: See website
			\item \textbf{Assignments}: posted on myCourses at least a week in advance
			\item \textbf{Questions}: Piazza
			\item \textbf{Programming language}: R or Python
			\pause
			\item \textbf{Plagiarism policy}: Not allowed. I encourage collaboration, but the homework must be prepared independently. It is OK to look online, but once you understand the solution, prepare it yourself.
		\end{itemize}
	\end{frame}

	\begin{frame} \frametitle{Course Texts}
		\begin{itemize}
		\item \textbf{Main Textbook}: James, G., Witten, D., Hastie, T., \& Tibshirani, R. (2013). An Introduction to Statistical Learning 
		\begin{itemize}
			\item PDF available online. 
		\end{itemize}
		\vfill
		\item \textbf{Secondary Textbook}: Bishop, C. M. (2006). Pattern Recognition and Machine Learning 
		\begin{itemize}
			\item Optional. Extra-credit and optional problems on assignments.
			\item PDF available online (see website)
		\end{itemize}
		\vfill
		\item See the class website for other books and online sources.
		\end{itemize}
	\end{frame}	

	\begin{frame} \frametitle{What is Machine Learning}
		\begin{itemize}
			\item Discover unknown function $\alert{f}$: 
			\[ \tcb{Y} = \tcr{f}(\tcg{X})  \]
			\item $\tcr{f}$ = \textbf{hypothesis}
			\item $\tcg{X}$ = \textbf{features}, or predictors, or inputs
			\item $\tcb{Y}$ = \textbf{response}, or target
		\end{itemize}	
		\[ \varname{MPG} = \alert{f}(\varname{Horsepower}) \]
		\begin{center}\includegraphics[width=0.5\linewidth]{../figs/class1/auto_function.pdf}\end{center}	
	\end{frame}

	\begin{frame} \frametitle{Machine Learning}
	\centering
		\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2cm,semithick,block/.style = {rounded corners, draw,fill=blue!1,align=center,inner sep=5}]
			\node[block](data){Dataset};
			\node[block,below of=data](algo){Machine Learning Algorithm};
			\node[block,below of=algo](hypo){Hypothesis $\tcr{f}$};
			\node[left of=hypo,xshift=-1cm](input) {Predictors $\tcg{X}$};
			\node[right of=hypo,xshift=1cm](output) {Target $\tcb{Y}$};
			\path (data) edge (algo) 
				  (algo) edge (hypo)
				  (input) edge [dashed] (hypo)
				  (hypo) edge [dashed] (output);
		\end{tikzpicture}
	\end{frame}

	\begin{frame} \frametitle{Auto Dataset}
		\centering
		\begin{tabular}{rrrl}
			\hline
			& \tcb{mpg} & \tcg{horsepower} & name \\ 
			\hline
			1 & 18.00 & 130.00 & chevrolet chevelle malibu \\ 
			2 & 15.00 & 165.00 & buick skylark 320 \\ 
			3 & 18.00 & 150.00 & plymouth satellite \\ 
			4 & 16.00 & 150.00 & amc rebel sst \\ 
			5 & 17.00 & 140.00 & ford torino \\ 
			6 & 15.00 & 198.00 & ford galaxie 500 \\ 
			7 & 14.00 & 220.00 & chevrolet impala \\ 
			8 & 14.00 & 215.00 & plymouth fury iii \\ 
			9 & 14.00 & 225.00 & pontiac catalina \\ 
			10 & 15.00 & 190.00 & amc ambassador dpl \\ 
			& $\cdots$ & $\cdots$ & $\cdots$ \\
			\hline
		\end{tabular}
	\end{frame}

	\begin{frame} \frametitle{Auto Dataset}
		\centering
		\begin{center}\includegraphics[width=0.9\linewidth]{../figs/class1/auto_data.pdf}\end{center}
	\end{frame}

	\begin{frame} \frametitle{Bayes Classifier}
		\alert{What is the \tcb{MPG} of a car with \tcg{horsepower} = 150?} \\
		\begin{center}
			\begin{tabular}{rrrl}
			\hline
			& \tcb{mpg} & \tcg{horsepower} & name \\ 
			\hline
			1 & 18.00 & 130.00 & chevrolet chevelle malibu \\ 
			2 & 15.00 & 165.00 & buick skylark 320 \\ 
			3 & 18.00 & 150.00 & plymouth satellite \\ 
			4 & 16.00 & 150.00 & amc rebel sst \\ 
			5 & 17.00 & 140.00 & ford torino \\ 
			6 & 15.00 & 198.00 & ford galaxie 500 \\ 
			7 & 14.00 & 220.00 & chevrolet impala \\ 
			8 & 14.00 & 215.00 & plymouth fury iii \\ 
			9 & 14.00 & 225.00 & pontiac catalina \\ 
			10 & 15.00 & 190.00 & amc ambassador dpl \\ 
			\hline
			\end{tabular}
		\end{center}
		\pause
		\[f(x) = \Ex [Y \mid X = x] \]
		\pause
		\begin{itemize}
			\item Achieves the minimum possible error with infinite data
			\item \alert{What if data is finite?}
		\end{itemize}		
	\end{frame}

	\begin{frame} \frametitle{Limitation of Bayes Classifier}	
		\alert{What is the \tcb{MPG} of a car with \tcg{horsepower} = 200?} \\
		\begin{center}
			\begin{tabular}{rrrl}
				\hline
				& \tcb{mpg} & \tcg{horsepower} & name \\ 
				\hline
				1 & 18.00 & 130.00 & chevrolet chevelle malibu \\ 
				2 & 15.00 & 165.00 & buick skylark 320 \\ 
				3 & 18.00 & 150.00 & plymouth satellite \\ 
				4 & 16.00 & 150.00 & amc rebel sst \\ 
				5 & 17.00 & 140.00 & ford torino \\ 
				6 & 15.00 & 198.00 & ford galaxie 500 \\ 
				7 & 14.00 & 220.00 & chevrolet impala \\ 
				8 & 14.00 & 215.00 & plymouth fury iii \\ 
				9 & 14.00 & 225.00 & pontiac catalina \\ 
				10 & 15.00 & 190.00 & amc ambassador dpl \\ 
				\hline
			\end{tabular}
		\end{center}
		\visible<2>{Return the \textbf{nearest neighbor}.}
	\end{frame}

	\begin{frame} \frametitle{Nearest Neighbor Hypothesis}	
		\begin{center}\includegraphics[width=\linewidth]{../figs/class1/auto_data_mean.pdf}\end{center}
	\end{frame}
	
	\begin{frame} \frametitle{Errors in Machine Learning}
		Must allow for errors $\epsilon$:
		\[ \tcb{Y} = \tcr{f}(\tcg{X}) + \epsilon \]			
		\pause
		\begin{enumerate}
		\item World is too complex to model precisely
		\item Many features are not captured in data sets
		\item Datasets are limited
		\end{enumerate}
	
		\begin{center}
			\visible<2>{\alert{How to reduce prediction errors?}}
		\end{center}
	\end{frame}
	

	\begin{frame} \frametitle{KNN: K-Nearest Neighbors}
		\textbf{Idea}: Use several similar training points when making predictions. Errors will average out.
		\begin{center}
			Example with 2 features (horsepower, model year) \\
			\includegraphics[width=0.9\linewidth]{{../islrfigs/Chapter2/2.14}.pdf}
		\end{center}	
	\end{frame}
	
	\begin{frame} \frametitle{KNN: Effect of different $k$}
		\begin{center}\includegraphics[width=\linewidth]{../figs/class1/auto_knn_compare.pdf}\end{center}	
	\end{frame}
	
	\begin{frame}\frametitle{Questions ...}
		\begin{itemize}
			\item How to choose $k$?
			\item Are there better methods?
		\end{itemize}
	\end{frame}

	\begin{frame} \frametitle{Parametric Prediction Methods}
		Linear regression:
		\[ \varname{MPG} = f(\varname{horsepower}) = \beta_0 + \beta_1 \times \varname{horsepower}  \]
		\begin{center}\includegraphics[width=0.8\linewidth]{../figs/class1/auto_data_regression.pdf}
		\end{center}
	\end{frame}

	\begin{frame} \frametitle{Machine Learning Choices ...}
		\centering
		\includegraphics[width=\linewidth]{../figs/class1/ml_map.png}\\
		{\tiny Source: \url{http://scikit-learn.org/stable/tutorial/machine_learning_map/index.html}} \\[3mm]
		\textbf{By May 15}: Know what, when, and why to use.
	\end{frame}


	\begin{frame}\frametitle{Why Estimate Hypothesis $f$?}
		\[ \varname{Sales} = f(\varname{TV}, \varname{Radio}, \varname{Newspaper}) \]
		\begin{center}\includegraphics[width=0.75\linewidth]{{../islrfigs/Chapter2/2.1}.pdf}\end{center}	
		\begin{enumerate}
			\item<2-> \textbf{Prediction}: Make predictions about future: Best medium mix to spend ad money?
			\item<3-> \textbf{Inference}: Understand the relationship: What kind of ads work? Why?
		\end{enumerate}
	\end{frame}

	\begin{frame} \frametitle{Prediction or Inference?}
		\begin{small}
			\begin{tabular}{|l|c|c|}
			\hline
			Application & Prediction & Inference \\
			\hline
			Identify risk factors for getting a disease &  & \visible<2->{\checkmark} \\
			Predict effectiveness of a treatment & \visible<3->{\checkmark} & \visible<3->{\checkmark} \\
			Recognize hand-written text & \visible<4->{\checkmark} & \\
			Speech recognition  & \visible<5->{\checkmark} & \\
			Determine why employees are leaving & \visible<6->{\checkmark} & \visible<6->{\checkmark} \\
			\hline
		\end{tabular}
	\end{small}
	\end{frame}

	\begin{frame}\frametitle{Types of Function $f$}
		\begin{columns}
			\begin{column}{0.5\linewidth}
				\centering
				\textbf{Regression}: continuous target
				\[ \tcr{f} : \tcg{\mathcal{X}} \rightarrow \tcb{\mathbb{R}} \]
				\begin{center}\includegraphics[width=0.9\linewidth]{{../islrfigs/Chapter2/2.6}.pdf}\end{center}	
			\end{column}
			\begin{column}{0.5\linewidth}
				\centering
				\textbf{Classification}: discrete target
				\[ \tcr{f} : \tcg{\mathcal{X}} \rightarrow \tcb{\{ 1,2,3,\ldots, k \}} \]
				\begin{center}\includegraphics[width=0.9\linewidth]{{../islrfigs/Chapter2/2.13}.pdf}\end{center}	
			\end{column}		
		\end{columns}
	\end{frame}
	
	\begin{frame} \frametitle{Regression or Classification?}
		\begin{small}
			\begin{tabular}{|l|c|c|}
				\hline
				Application & Regression & Classification \\
				\hline
				Identify risk of getting a disease & \visible<2->{\checkmark} &  \\
				Predict effectiveness of a treatment & \visible<3->{\checkmark} &  \\
				Recognize hand-written text &  & \visible<4->{\checkmark} \\
				Speech recognition  &  & \visible<5->{\checkmark}\\
				Predict probability of an employee leaving & \visible<6->{\checkmark} &  \\
				\hline
			\end{tabular}
		\end{small}
	\end{frame}

	\begin{frame} \frametitle{Prediction Error: Training and test data sets}
		\centering
		\begin{tabular}{rrrll}
		\hline
		& \tcb{mpg} & \tcg{horsepower} & name & \\ 
		\hline
		1 & 18.00 & 130.00 & chevrolet chevelle malibu & \rdelim\}{3}{3mm}[test] \\ 
		2 & 15.00 & 165.00 & buick skylark 320 &\\ 
		3 & 18.00 & 150.00 & plymouth satellite &\\ 
		\hline \hline
		4 & 16.00 & 150.00 & amc rebel sst & \rdelim\}{7}{3mm}[training] \\ 
		5 & 17.00 & 140.00 & ford torino & \\ 
		6 & 15.00 & 198.00 & ford galaxie 500 & \\ 
		7 & 14.00 & 220.00 & chevrolet impala & \\ 
		8 & 14.00 & 215.00 & plymouth fury iii & \\ 
		9 & 14.00 & 225.00 & pontiac catalina & \\ 
		10 & 15.00 & 190.00 & amc ambassador dpl & \\ 
		\hline
	\end{tabular}
	\end{frame}

	\begin{frame}\frametitle{How Good are Predictions?}
	\begin{enumerate}
		\item Split data into \textbf{train} and \textbf{test} sets
		\item Learn function $\hat{f}$ using only \textbf{training} data
		\item Test data: ${(x_1,y_1), (x_2, y_2), \ldots}$
		\item Compute \textbf{Mean Squared Error (MSE)} on \textbf{test} data: 
		\[ \varname{MSE} = \frac{1}{n} \sum_{i=1}^{n} (y_i - \hat{f}(x_i))^2  \]
	\end{enumerate}
		\textbf{Important assumption}: Samples $x_i$ are i.i.d.
	\end{frame}
	
	\begin{frame} \frametitle{Do We Need Test Data?}
	\begin{itemize}
		\item Why not just test on the training data?
		\begin{center}
			KNN Error
			\includegraphics[width=0.9\linewidth]{{../islrfigs/Chapter2/2.17}.pdf}	
		\end{center}	
		\item Small $k$ is flexible and overfits
	\end{itemize}
	\end{frame}
	
	\begin{frame}\frametitle{Why Methods Fail: Bias-Variance Decomposition}
		\[ Y = f(X) + \alert{\epsilon} \]			
		Mean Squared Error of trained $\hat{f}$ can be decomposed as:
		\[ \varname{MSE} = \Ex [(Y - \hat{f}(X))^2] = \underbrace{\Var[\hat{f}(X)]}_{\text{\tcg{Variance}}} + \underbrace{\Ex[\hat{f}(X) - f(X)]^2}_{\text{\tcb{Bias}$^2$}} + \Var[\tcr{\epsilon}] \]
		\begin{itemize}
		\item \textbf{\tcb{Bias}}: How well would method work with average dataset
		\item \textbf{\tcg{Variance}}: How much does output change with different data sets
		\end{itemize}
	\end{frame}
	
	\begin{frame} \frametitle{Bias-Variance Trade-off}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter2/2.12}.pdf}\end{center}
	\end{frame}	

	\begin{frame} \frametitle{R Language}
		\begin{itemize}
			\item Download and install R: \url{http://cran.r-project.prg}
			\item Try using RStudio as an R IDE
			\item \emph{See class website for more info}
			\item Read the R lab: ISL 2.3
			\item Use Piazza for questions
			\item All my R code is in the class git repo, suggestions and bug fixes are welcome
		\end{itemize}
	\end{frame}

	\begin{frame} \frametitle{Alternatives to R}
		\begin{itemize}
			\item \textbf{Python} (+ Numpy + Pandas + Matplotlib + Scikits Learn + Scipy) \\
				Popular, nicer language, more hassle
			\item \textbf{MATLAB} \\
				Linear algebra and control focus, but a lot of ML toolkits
			\item \textbf{Julia} \\
				New technical computing language, ``walks like python runs like c'', but least mature
		\end{itemize}
	\end{frame}

	\begin{frame}{Why R (vs Python)}
		\begin{enumerate}
			\item Language syntax particularly suitable for manipulating tabular data
			\item Better-quality packages at \url{cran.r-project.org} than \url{pypi.org}
			\item Excellent data manipulation and visualization tools: dplyr, ggplot, tidyverse, better than python versions like pandas, matplotlib 
			\item R-notebooks more flexible and git-friendly than Jupyter
			\item Shiny: a neat web framework to create simple web data interface
			\item Rcpp: convenient interface with C++
			\item Easier to install and use particularly on Windows/Mac
		\end{enumerate}	
	\end{frame}

	\begin{frame}{Why Python (vs R)}
		\begin{enumerate}
			\item Better general programming language, good support for data structures
			\item Better support for numerical algebra: numpy, scipy, 
			\item Better support for deep learning: tensor flow, keras
			\item Clean (but boring?) syntax
		\end{enumerate}
	\end{frame}

	\begin{frame}{Why Neither Python or R}
		\begin{enumerate}
			\item Their native code is very slow
			\item Flexible but error-prone syntax
		\end{enumerate}
	\end{frame}

	\begin{frame} \frametitle{What is Next ...}
		\begin{itemize}
			\item \textbf{Recitations} on Friday: cover homework assignments and R/Python help
			\item Assignment 0 due in on Monday 11:59PM
			\item Grades, assignments, etc on mycourses
			\item Other information:\\ {\small \url{https://gitlab.com/marekpetrik/ml-spring2020}}
			\item Last year's class: \\ {\small \url{https://gitlab.com/marekpetrik/ML-Spring2019}}
		\end{itemize}

	\end{frame}

\end{document}
