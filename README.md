# CS750 / CS850: Machine Learning #

### When and Where 

- *Mon* & *Wed*: 11:10 am - 12:30 pm in *McConnell 220*
- *Fri*: Recitation, 1:10 pm - 2:00 pm in *McConnell 220*

See [class overview](overview/overview.pdf) for the information on grading, rules, and office hours.

## Syllabus: Lectures

The slides will be updated before each lecture. The topics are preliminary and may change due to snow days or due to popular demand.

Regarding *reading materials* see the section on textbooks below. *ISL* is the main textbook, there will be optinal assignments from *PRM*. 

| Date  | Day | Slides                                                           | Reading     | Notebooks
| ----- | --- | ---------------------------------------------------------------- | ----------- | ---------
| Jan 22| Wed | [Introduction](slides/class1/class1.pdf)                         | ISL 1,2     | [(RMD)](notebooks/class1/introduction.Rmd) [Plots](slides/figs/class1/plots.R)
| Jan 27| Mon | [Linear regression I](slides/class2/class2.pdf)                  | ISL 3.1-2   |
| Jan 29| Wed | [Linear regression II](slides/class3/class3.pdf)                 | ISL 3.3-6   |
| Feb 03| Mon | [Logistic regression](slides/class4/class4.pdf)                  | ISL 4.1-3   |
| Feb 05| Wed | [LDA, QDA](slides/class5/class5.pdf) 		                       | ISL 4.4-6   |
| Feb 10| Mon | [Maximum likelihood, MAP](slides/class6/class6.pdf)              |             |
| Feb 12| Wed | [Maximum likelihood, MAP](slides/class6/class6.pdf)              |             |
| Feb 17| Mon | [Cross-validation](slides/class7/class7.pdf)                     | ISL 5       |
| Feb 19| Wed | [Model selection](slides/class8/class8.pdf)                      | ISL 6.1-6.2 |
| Feb 24| Mon | [Dimensionality](slides/class9/class9.pdf)                       | ISL 6.3-6.4 |
| Feb 26| Wed | [Regression splines](slides/class10/class10.pdf)                 | ISL 7       |
| Mar 02| Mon | [Gradient descent, Newton's Method](slides/class6_5/class6_5.pdf)| CO 9.2      | [(Optimization)](notebooks/class6_5/optimization.Rmd)
| Mar 04| Wed | [Linear algebra](slides/class11_5/class11_5.pdf)                 | LAR         |
| Mar 09| Mon | [Linear algebra and ML](slides/class12/class12.pdf)              | LAR         |
| Mar 11| Wed | [Miniproject and Midterm review](slides/class11/class11.pdf)     |             |
| Mar 16| Mon | *Spring Break*                                                   |             |
| Mar 18| Wed | *Spring Break*                                                   |             |
| Mar 23| Mon | [Decision trees and boosting](slides/class13/class13.pdf)        | ISL 8       |
| Mar 25| Wed | [SVM](slides/class14/class14.pdf)                                | ISL 9       |
| Mar 30| Mon | [SVM, Project](slides/class15/class15.pdf)                       | ISL 9       |
| Apr 01| Wed | [PCA](slides/class16/class16.pdf)                                | ISL 10.1-2  |
| Apr 06| Mon | [Clustering](slides/class17/class17.pdf)                         | ISL 10.3-5  |
| Apr 08| Wed | [Bayesian machine learning](slides/class19/class19.pdf)          | AIMA 14     |
| Apr 13| Mon | [Bayesian machine learning](slides/class19/class19.pdf)          | AIMA 14     |
| Apr 15| Wed | [Deep learning](slides/class20/class20.pdf)                      | DL          |
| Apr 20| Mon | [Convolutional & Recurrent NN](slides/class21/class21.pdf)       | DL          |
| Apr 22| Wed | [Adversarial Deep Learning](slides/class21/class21_more.pdf)     | DL          |
| Apr 27| Mon | [Empirical risk minimization]()                                  |             |
| Apr 29| Wed | [Final exam review](slides/class23/class23.pdf)                  |             |
| May 04| Mon | [Project presentations and discussion](#)|            | 

The recitations will cover current assignments, answer questions, and show demos. Neither the lectures nor the recitations are mandatory.

## Assignments

*Piazza*: [piazza.com/unh/spring2020/cs750cs850](http://piazza.com/unh/spring2020/cs750cs850)

*Office Hours*:

  - *Marek*: Wed 1:30-3:00pm in Kingsbury N215b
  - *Soheil (TA)*: Mon 2-4pm in Kingsbury W240
  - *Xihong (TA)*: Thu 1:30-2:30pm in Kingsbury W244

The dates below are tentative.

| Assignment                        | Source                             |  Due Date             |
|---------------------------------- | ---------------------------------- | ----------------------|
| [0](assignments/assignment0.pdf)  | [0](assignments/assignment0.Rmd)   | Mon 1/27/19 at 11:59PM|

## Solutions

Coming soon.

## Mini-project

Coming soon.

## Project

Coming soon.

## Quizzes

The projected dates for the quizzes are:

| Quiz          | Notes | Dates Available|
| ------------- | ----- | -------------- |
| Quiz 1        |       | 2/18-2/25      |
| Quiz 2        |       | 3/05-3/18      |
| Quiz 3        |       | 4/08-4/15      |
| Quiz 4        |       | 4/25-5/05      |

The quizzes will be available online on mycourses. See [practice questions](questions/test_questions.pdf) for the type of questions you may expect to be on the quizzes.

The quizzes are multiple choice and there will be 3 attempts allowed for each quiz.

## Exams

The final exam is currently scheduled to be during the finals week. The exam will be based on conceptual/theoretical questions similar to homework assignments.

## Textbooks ##

### Main References:
- **ISL**: [James, G., Witten, D., Hastie, T., & Tibshirani, R. (2013). An Introduction to Statistical Learning](http://www-bcf.usc.edu/~gareth/ISL/)
- **PRM** [Bishop, C. M. (2006). Pattern Recognition and Machine Learning]() *For optional assignment questions for extra-credit*

### In-depth Machine Learning:
- **ESL**: [Hastie, T., Tibshirani, R., & Friedman, J. (2009). The Elements of Statistical Learning. Springer Series in Statistics (2nd ed.)](http://statweb.stanford.edu/~tibs/ElemStatLearn)
- **MLP**: Murphy, K (2012). Machine Learning, A Probabilistic Perspective.
- **ML**: Mitchell, T. (1997). Machine Learning
- Scholkopf B., Smola A. (2001). Learning with Kernels.
- **DL**: Goodfellow, I., Bengio, Y., & Courville, A. (2016). [Deep Learning](http://www.deeplearningbook.org/) 

### Linear Algebra:
- **LAO**: Hefferon, J. [Linear Algebra](http://joshua.smcvt.edu/linearalgebra/#current_version) (2017)
- **LA**: Strang, G. [Introduction to Linear Algebra](http://math.mit.edu/~gs/linearalgebra/). (2016) *Also see:* [online lectures](https://ocw.mit.edu/courses/mathematics/18-06-linear-algebra-spring-2010/video-lectures/)
- **LAR**: [Introductory Linear Agebra with R](http://bendixcarstensen.com/APC/linalg-notes-BxC.pdf)

### Mathematical Optimization:
- **CO** Boyd, S., & Vandenberghe, L. (2004). [Convex Optimization](http://web.stanford.edu/~boyd/cvxbook/)
- Nocedal, J., Wright, S. (2006). [Numerical Optimization](https://www.springer.com/us/book/9780387303031)

### Statistics:
- Casella G., & Berger R. L. (2002) Statistical Inference, 2nd edition.

### Related Areas:
- **RL**: Sutton, R. S., & Barto, A. (2018). [Reinforcement learning](http://people.inf.elte.hu/lorincz/Files/RL_2006/SuttonBook.pdf). 2nd edition 
- **RLA**: Szepesvari, C. (2013), [Algorithms for Reinforcement Learning](https://sites.ualberta.ca/~szepesva/RLBook.html)
- **AIMA**: Russell, S., & Norvig, P. (2013). Artificial Intelligence A Modern Approach. (3rd ed.).


## Class Content

The goal of this class is to teach you how to use *machine learning* to *understand data* and *make predictions* in practice. The class will cover the fundamental concepts and algorithms in machine learning and data science as well as a wide variety of practical algorithms. The main topics we will cover are:

1. The maximum likelihood principle
2. *Regression*: Linear regression
3. *Classification*: Logistic regression and linear discriminant analysis
4. *Cross-validation*: bootstrap, and over-fitting
5. *Model selection*: Regularization, Lasso
6. *Nonlinear models*: Decision trees, Support vector machines
7. *Unsupervised*: Principal component analysis, k-means
8. *Advanced topics*: Bayes nets and deep learning

The graduate version of the class will cover the same topics in more depth.

## Programming Language

The class will involve hand-on data analysis using machine learning methods. The recommended language for programming assignments is [R](https://www.r-project.org/) which is an excellent tool for statistical analysis and machine learning. *No prior knowledge of R is needed or expected*; the book and lectures will include a gentle introduction to the language. You can also use Python instead if you are confident that you can install appropriate packages and figure out how to solve assignments. About 15% of the class used Python in 2019.

### R Resources

- [R For Data Science](https://r4ds.had.co.nz/index.html)
- [Cheatsheets](https://www.rstudio.com/resources/cheatsheets/)
- [R Markdown](https://github.com/rstudio/cheatsheets/raw/master/rmarkdown-2.0.pdf)

We recommend using the free [R Studio](https://www.rstudio.com) for completing programming assignments. R Notebooks are very convenient for producing reproducible reports and we encourage you to use them.

### Python Resources

Book code is available for Python, for example, [here](https://github.com/JWarmenhoven/ISLR-python). If you find other good sources, please let me know. [Jupyter](https://jupyter.org/) is a similar alternative for Python. 

## Pre-requisites ##

Basic programming skills (scripting languages like Python are OK) and some familiarity with statistics and calculus. If in doubt, please email the instructor.
